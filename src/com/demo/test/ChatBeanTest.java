package com.demo.test;

import junit.framework.TestCase;

import javax.annotation.Resource;
import javax.ejb.embeddable.EJBContainer;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

public class ChatBeanTest extends TestCase {

    @Resource
    private ConnectionFactory connectionFactory;

    @Resource(name = "ChatBean")
    private Queue questionQueue;

    @Resource(name = "AnswerQueue")
    private Queue answerQueue;
    
    private final String HOLA = "Hola";
    private final String REPUESTA_SALUDO = "Hola, Soy Java";
    private final String COMO_ESTAS = "¿Cómo estás?";
    private final String RESPUESTA_COMO_ESTAS = "Estoy bien";
    private final String SIGUES_PROGRAMANDO = "¿Sigues programando?";
    private final String RESPUESTA_SIGUES_PROGRAMANDO = "Si, todos los días";

    public void test() throws Exception {
        EJBContainer.createEJBContainer().getContext().bind("inject", this);


        final Connection connection = connectionFactory.createConnection();

        connection.start();

        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        final MessageProducer questions = session.createProducer(questionQueue);

        final MessageConsumer answers = session.createConsumer(answerQueue);

        
        sendText(HOLA, questions, session);
        

        assertEquals(REPUESTA_SALUDO, receiveText(answers));


        sendText(COMO_ESTAS, questions, session);

        assertEquals(RESPUESTA_COMO_ESTAS, receiveText(answers));


        sendText(SIGUES_PROGRAMANDO, questions, session);

        assertEquals(RESPUESTA_SIGUES_PROGRAMANDO, receiveText(answers));
    }

    private void sendText(String text, MessageProducer questions, Session session) throws JMSException {
    	System.out.println("Pregunta: " + text);
        questions.send(session.createTextMessage(text));
    }

    private String receiveText(MessageConsumer answers) throws JMSException {
    	String respuesta = ((TextMessage) answers.receive(1000)).getText();
    	System.out.println("Respuesta: " + respuesta);
        return respuesta;
    }
    
   
}