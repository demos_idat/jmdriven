package com.demo;

import javax.annotation.Resource;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

@MessageDriven
public class ChatBean implements MessageListener {

    @Resource
    private ConnectionFactory connectionFactory;

    @Resource(name = "AnswerQueue")
    private Queue answerQueue;
    
    private final String HOLA = "Hola";
    private final String REPUESTA_SALUDO = "Hola, Soy Java";
    private final String COMO_ESTAS = "¿Cómo estás?";
    private final String RESPUESTA_COMO_ESTAS = "Estoy bien";
    private final String SIGUES_PROGRAMANDO = "¿Sigues programando?";
    private final String RESPUESTA_SIGUES_PROGRAMANDO = "Si, todos los días";
    	

    public void onMessage(Message message) {
        try {

            final TextMessage textMessage = (TextMessage) message;
            final String question = textMessage.getText();

            if (HOLA.equals(question)) {

                respond(REPUESTA_SALUDO);
            } else if (COMO_ESTAS.equals(question)) {

                respond(RESPUESTA_COMO_ESTAS);
            } else if (SIGUES_PROGRAMANDO.equals(question)) {

                respond(RESPUESTA_SIGUES_PROGRAMANDO);
            }
        } catch (JMSException e) {
            throw new IllegalStateException(e);
        }
    }

    private void respond(String text) throws JMSException {

        Connection connection = null;
        Session session = null;

        try {
            connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create a MessageProducer from the Session to the Topic or Queue
            MessageProducer producer = session.createProducer(answerQueue);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            // Create a message
            TextMessage message = session.createTextMessage(text);

            // Tell the producer to send the message
            producer.send(message);
        } finally {
            // Clean up
            if (session != null) session.close();
            if (connection != null) connection.close();
        }
    }
}
